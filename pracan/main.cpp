#include "llvm/Support/Host.h"
#include "llvm/IR/LLVMContext.h"
#include "llvm/IR/Metadata.h"
#include "llvm/IR/Module.h"
#include "llvm/IRReader/IRReader.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/Pass.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/Support/SourceMgr.h"
#include "llvm/IR/CFG.h"
#include "llvm/IR/Instruction.h"
#include "llvm/IR/InstIterator.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/Value.h"
#include "llvm/ADT/ilist.h"
#include "llvm/ADT/MapVector.h"

#include <list>
#include <unordered_map>
#include <algorithm>
#include <set>

using namespace llvm;
using namespace std;

enum direction_t {
    forward,
    backward
};

template<class T>
void print_set(const set<T> &s) {
    for (int el : s) {
        outs() << el << " ";
    }
}

template<class T>
void print_vect(const vector<T> &v) {
    for (int el : v) {
        outs() << el << " ";
    }
}

int bb_to_id(const BasicBlock &bbp) {
    string str_id = bbp.getName();
    return atoi(str_id.c_str());
}

// set block names of function as integer id and build a vector of these ids 
void build_blocks_ids(Function &function, vector<int> &block_ids) {
    // block ids counter, will be used as id
    int blocks_counter = -1;

    for (BasicBlock &bb : function) {
        blocks_counter++;
        
        // set name of the current basic block
        std::string s = std::to_string(blocks_counter);
        const char *cur_name = s.c_str(); 
        bb.setName(string(cur_name));
        
        block_ids.push_back(blocks_counter); 
    }
}

// find Pred(B) ids for each B in the function. The result vector's order corresponds to the order of getBasicBlockList iterator
void find_preds(Function &function, vector<vector<int>> &pred_ids) {
    for (BasicBlock &bb : function) {
        vector<int> cur_pred;
        for (pred_iterator pr_it = pred_begin(&bb), E = pred_end(&bb); pr_it != E; ++pr_it) {
             cur_pred.push_back(bb_to_id(**pr_it));
        }
        pred_ids.push_back(cur_pred);
    }
}

// find Succ(B) ids for each B in the function. The result vector's order corresponds to the order of getBasicBlockList iterator
void find_succs(Function &function, vector<vector<int>> &succ_ids) {
    for (BasicBlock &bb : function) {
        vector<int> cur_succ;
        for (succ_iterator succ_it = succ_begin(&bb), E = succ_end(&bb); succ_it != E; ++succ_it) {
             cur_succ.push_back(bb_to_id(**succ_it));
        }
        succ_ids.push_back(cur_succ);
    }
}

bool isEntry(BasicBlock &bb, Function &function, direction_t d) {
    if (d == direction_t::forward) {
        return bb.getName() == function.front().getName();
    } else {
        return bb.getName() == function.back().getName();
    }
}

// boundary value of Entry in finding dominators is Entry's id
void dominators_boundary_func(set<int> &res, const BasicBlock &bb, const vector<int> &block_ids) {
    res.insert(bb_to_id(bb));
}

// initial value of any block in finding dominators is all blocks
void dominators_initial_func(set<int> &res, const BasicBlock &bb, const vector<int> &block_ids) {
    for (int i : block_ids) {
        res.insert(i);
    }
}

void dominators_lattice(set<int> &res, const set<int> &arg1, const set<int> &arg2) {
    res.clear();
    set_intersection(arg1.begin(), arg1.end(), arg2.begin(), arg2.end(), inserter(res, res.end()));
}

void dominators_transfer_func(set<int> &res, const BasicBlock &bb, const set<int> &arg) {
    res = arg;
    res.insert(bb_to_id(bb));
}

template<class A>
void most_generalized_iterative_algorithm(Function &function,
                       void (*boundary_func)(A &res, const BasicBlock &bb, const vector<int> &block_ids),
                       void (*initial_func)(A &res, const BasicBlock &bb, const vector<int> &block_ids),
                       void (*lattice)(A &res, const A &arg1, const A &arg2),
                       void (*transfer_func)(A &res, const BasicBlock &bb, const A &arg),
                       direction_t dir,
                       vector<A> &gen_inputs, vector<A> &gen_outputs) { // gen_inputs and gen_outputs correspond to forward direction
    int i = 0;
    vector<int> block_ids;
    // successors or predessors
    vector<vector<int>> neighbours;

    build_blocks_ids(function, block_ids);
    if (dir == direction_t::forward) {
        find_preds(function, neighbours);
    } else {
        find_succs(function, neighbours);
    }

    // allocate inputs and outputs
    int n_blocks = block_ids.size();
    gen_inputs.resize(n_blocks);
    gen_outputs.resize(n_blocks);

    // initialization: set boundary value to the entry and initial to other blocks
    for (BasicBlock &bb : function) {
        if (isEntry(bb, function, dir)) {
            boundary_func(gen_outputs[i], bb, block_ids);
        }
        else {
            initial_func(gen_outputs[i], bb, block_ids);
        }
        i++;
    }

    bool gen_in_has_changed = true;
    while(gen_in_has_changed) {
        i = -1;
        gen_in_has_changed = false;
        for (BasicBlock &bb : function) {
            i++;
            if (isEntry(bb, function, dir))
                continue;

            // calculating new Output[i]
            if (neighbours[i].size() == 1) {
                gen_inputs[i] = gen_outputs[neighbours[i][0]];
            } else if (neighbours[i].size() == 2) {
                lattice(gen_inputs[i], gen_outputs[neighbours[i][0]], gen_outputs[neighbours[i][1]]);
            }

            // calculating new Input[i]
            A old_gen_output(gen_outputs[i]);
            transfer_func(gen_outputs[i], bb, gen_inputs[i]);

            gen_in_has_changed |= gen_outputs[i] != old_gen_output;
        }
    }
}

template<class A>
void generalized_iterative_algorithm(Function &function,
                       void (*boundary_func)(A &res, const BasicBlock &bb, const vector<int> &block_ids),
                       void (*initial_func)(A &res, const BasicBlock &bb, const vector<int> &block_ids),
                       void (*lattice)(A &res, const A &arg1, const A &arg2),
                       void (*transfer_func)(A &res, const BasicBlock &bb, const A &arg),
                       direction_t dir,
                       vector<A> &inputs, vector<A> &outputs) {
    if (dir == direction_t::forward)
        most_generalized_iterative_algorithm(function, boundary_func, initial_func, lattice, transfer_func, dir, inputs, outputs);
    else
        most_generalized_iterative_algorithm(function, boundary_func, initial_func, lattice, transfer_func, dir, outputs, inputs);
}

int main(int argc, char **argv) {
    if (argc < 2) {
        errs() << "usage: main.out <file_to_analyze>\n";
        return 1;
    }

    SMDiagnostic err;
    std::unique_ptr<Module> m(ParseIRFile(argv[1], err, getGlobalContext()));
    if (!m) {
        err.print(argv[0], errs());
        return 1;
    }
    
    // iterate over functions
    for(Module::iterator f = m->begin(), E = m->end(); f != E; ++f) {
        outs()<<"function:"<<f->getName()<<"\n";

        // vector of basic blocks integer ids
        vector<int> block_ids;
        build_blocks_ids(*f, block_ids);
        outs() << "Basic blocks found: " << block_ids.size() << "\n\n";

        // find dominators
        vector<set<int>> inputs;
        vector<set<int>> outputs;
        generalized_iterative_algorithm(*f, &dominators_boundary_func, &dominators_initial_func,
                          &dominators_lattice, &dominators_transfer_func, direction_t::forward, inputs, outputs);
        for (int i = 0; i < outputs.size(); i++) {
            outs() << "dominators of block B" << block_ids[i] << ": ";
            print_set(outputs[i]);
            outs() << "\n";
        }
    }
  return 0;
}

