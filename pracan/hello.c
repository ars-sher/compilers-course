#include <stdio.h>
#include <stdlib.h>

int main() {
  int i;
  int j;
  srand(time(NULL));
  i = rand() % 10;
  if (i < 5) {
    printf("Hello left world! i =  %d\n", i);
  }
  else {
    printf("Hello right world! i =  %d\n", i);
  }
  j = i + 3;
  printf("Goodbye, World!\n");
  return 0;
}
