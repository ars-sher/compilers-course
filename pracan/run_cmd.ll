; ModuleID = 'edolymp5/run_cmd.c'
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

%union.__WAIT_STATUS = type { %union.wait* }
%union.wait = type { i32 }
%union.anon = type { i32 }

@.str = private unnamed_addr constant [23 x i8] c"Error: forking failed\0A\00", align 1
@.str1 = private unnamed_addr constant [33 x i8] c"Error: failed to run command %s\0A\00", align 1

; Function Attrs: nounwind uwtable
define i32 @run_cmd(i8* %cmd_name) #0 {
  %1 = alloca i32, align 4
  %2 = alloca i8*, align 8
  %pid = alloca i32, align 4
  %argv = alloca [2 x i8*], align 16
  store i8* %cmd_name, i8** %2, align 8
  %3 = load i8** %2, align 8
  %4 = getelementptr inbounds [2 x i8*]* %argv, i32 0, i64 0
  store i8* %3, i8** %4, align 8
  %5 = getelementptr inbounds [2 x i8*]* %argv, i32 0, i64 1
  store i8* null, i8** %5, align 8
  %6 = call i32 @fork() #4
  store i32 %6, i32* %pid, align 4
  %7 = icmp slt i32 %6, 0
  br i1 %7, label %8, label %10

; <label>:8                                       ; preds = %0
  %9 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([23 x i8]* @.str, i32 0, i32 0))
  store i32 -1, i32* %1
  br label %27

; <label>:10                                      ; preds = %0
  %11 = load i32* %pid, align 4
  %12 = icmp eq i32 %11, 0
  br i1 %12, label %13, label %24

; <label>:13                                      ; preds = %10
  %14 = getelementptr inbounds [2 x i8*]* %argv, i32 0, i64 0
  %15 = load i8** %14, align 8
  %16 = getelementptr inbounds [2 x i8*]* %argv, i32 0, i32 0
  %17 = call i32 @execvp(i8* %15, i8** %16) #4
  %18 = icmp slt i32 %17, 0
  br i1 %18, label %19, label %23

; <label>:19                                      ; preds = %13
  %20 = getelementptr inbounds [2 x i8*]* %argv, i32 0, i64 0
  %21 = load i8** %20, align 8
  %22 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([33 x i8]* @.str1, i32 0, i32 0), i8* %21)
  call void @exit(i32 1) #5
  unreachable

; <label>:23                                      ; preds = %13
  br label %24

; <label>:24                                      ; preds = %23, %10
  br label %25

; <label>:25                                      ; preds = %24
  %26 = load i32* %pid, align 4
  store i32 %26, i32* %1
  br label %27

; <label>:27                                      ; preds = %25, %8
  %28 = load i32* %1
  ret i32 %28
}

; Function Attrs: nounwind
declare i32 @fork() #1

declare i32 @printf(i8*, ...) #2

; Function Attrs: nounwind
declare i32 @execvp(i8*, i8**) #1

; Function Attrs: noreturn nounwind
declare void @exit(i32) #3

; Function Attrs: nounwind uwtable
define i32 @count_sgn_terminated(i32 %n) #0 {
  %1 = alloca i32, align 4
  %status = alloca i32, align 4
  %i = alloca i32, align 4
  %sgn_terminated_counter = alloca i32, align 4
  %2 = alloca %union.__WAIT_STATUS, align 8
  %3 = alloca %union.anon, align 4
  store i32 %n, i32* %1, align 4
  store i32 0, i32* %sgn_terminated_counter, align 4
  store i32 0, i32* %i, align 4
  br label %4

; <label>:4                                       ; preds = %27, %0
  %5 = load i32* %i, align 4
  %6 = load i32* %1, align 4
  %7 = icmp slt i32 %5, %6
  br i1 %7, label %8, label %30

; <label>:8                                       ; preds = %4
  %9 = bitcast %union.__WAIT_STATUS* %2 to i32**
  store i32* %status, i32** %9, align 8
  %10 = getelementptr %union.__WAIT_STATUS* %2, i32 0, i32 0
  %11 = load %union.wait** %10
  %12 = call i32 @wait(%union.wait* %11)
  %13 = bitcast %union.anon* %3 to i32*
  %14 = load i32* %status, align 4
  store i32 %14, i32* %13, align 4
  %15 = bitcast %union.anon* %3 to i32*
  %16 = load i32* %15, align 4
  %17 = and i32 %16, 127
  %18 = add nsw i32 %17, 1
  %19 = trunc i32 %18 to i8
  %20 = sext i8 %19 to i32
  %21 = ashr i32 %20, 1
  %22 = icmp sgt i32 %21, 0
  br i1 %22, label %23, label %26

; <label>:23                                      ; preds = %8
  %24 = load i32* %sgn_terminated_counter, align 4
  %25 = add nsw i32 %24, 1
  store i32 %25, i32* %sgn_terminated_counter, align 4
  br label %26

; <label>:26                                      ; preds = %23, %8
  br label %27

; <label>:27                                      ; preds = %26
  %28 = load i32* %i, align 4
  %29 = add nsw i32 %28, 1
  store i32 %29, i32* %i, align 4
  br label %4

; <label>:30                                      ; preds = %4
  %31 = load i32* %sgn_terminated_counter, align 4
  ret i32 %31
}

declare i32 @wait(%union.wait*) #2

; Function Attrs: nounwind uwtable
define i32 @run_cmd_sequential(i8* %cmd_name) #0 {
  %1 = alloca i32, align 4
  %2 = alloca i8*, align 8
  %pid = alloca i32, align 4
  %status = alloca i32, align 4
  %argv = alloca [2 x i8*], align 16
  store i8* %cmd_name, i8** %2, align 8
  %3 = load i8** %2, align 8
  %4 = getelementptr inbounds [2 x i8*]* %argv, i32 0, i64 0
  store i8* %3, i8** %4, align 8
  %5 = getelementptr inbounds [2 x i8*]* %argv, i32 0, i64 1
  store i8* null, i8** %5, align 8
  %6 = call i32 @fork() #4
  store i32 %6, i32* %pid, align 4
  %7 = icmp slt i32 %6, 0
  br i1 %7, label %8, label %10

; <label>:8                                       ; preds = %0
  %9 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([23 x i8]* @.str, i32 0, i32 0))
  store i32 -1, i32* %1
  br label %30

; <label>:10                                      ; preds = %0
  %11 = load i32* %pid, align 4
  %12 = icmp eq i32 %11, 0
  br i1 %12, label %13, label %24

; <label>:13                                      ; preds = %10
  %14 = getelementptr inbounds [2 x i8*]* %argv, i32 0, i64 0
  %15 = load i8** %14, align 8
  %16 = getelementptr inbounds [2 x i8*]* %argv, i32 0, i32 0
  %17 = call i32 @execvp(i8* %15, i8** %16) #4
  %18 = icmp slt i32 %17, 0
  br i1 %18, label %19, label %23

; <label>:19                                      ; preds = %13
  %20 = getelementptr inbounds [2 x i8*]* %argv, i32 0, i64 0
  %21 = load i8** %20, align 8
  %22 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([33 x i8]* @.str1, i32 0, i32 0), i8* %21)
  call void @exit(i32 1) #5
  unreachable

; <label>:23                                      ; preds = %13
  br label %27

; <label>:24                                      ; preds = %10
  %25 = load i32* %pid, align 4
  %26 = call i32 @waitpid(i32 %25, i32* %status, i32 0)
  br label %27

; <label>:27                                      ; preds = %24, %23
  br label %28

; <label>:28                                      ; preds = %27
  %29 = load i32* %pid, align 4
  store i32 %29, i32* %1
  br label %30

; <label>:30                                      ; preds = %28, %8
  %31 = load i32* %1
  ret i32 %31
}

declare i32 @waitpid(i32, i32*, i32) #2

attributes #0 = { nounwind uwtable "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nounwind "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { noreturn nounwind "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { nounwind }
attributes #5 = { noreturn nounwind }

!llvm.ident = !{!0}

!0 = metadata !{metadata !"clang version 3.5.2 (branches/release_35 232902)"}
