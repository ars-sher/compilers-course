; ModuleID = 'hello.c'
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

@.str = private unnamed_addr constant [27 x i8] c"Hello left world! i =  %d\0A\00", align 1
@.str1 = private unnamed_addr constant [28 x i8] c"Hello right world! i =  %d\0A\00", align 1
@.str2 = private unnamed_addr constant [17 x i8] c"Goodbye, World!\0A\00", align 1

; Function Attrs: nounwind uwtable
define i32 @main() #0 {
  %1 = alloca i32, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  store i32 0, i32* %1
  %2 = call i32 (i8*, ...)* bitcast (i32 (...)* @time to i32 (i8*, ...)*)(i8* null)
  call void @srand(i32 %2) #3
  %3 = call i32 @rand() #3
  %4 = srem i32 %3, 10
  store i32 %4, i32* %i, align 4
  %5 = load i32* %i, align 4
  %6 = icmp slt i32 %5, 5
  br i1 %6, label %7, label %10

; <label>:7                                       ; preds = %0
  %8 = load i32* %i, align 4
  %9 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([27 x i8]* @.str, i32 0, i32 0), i32 %8)
  br label %13

; <label>:10                                      ; preds = %0
  %11 = load i32* %i, align 4
  %12 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([28 x i8]* @.str1, i32 0, i32 0), i32 %11)
  br label %13

; <label>:13                                      ; preds = %10, %7
  %14 = load i32* %i, align 4
  %15 = add nsw i32 %14, 3
  store i32 %15, i32* %j, align 4
  %16 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([17 x i8]* @.str2, i32 0, i32 0))
  ret i32 0
}

; Function Attrs: nounwind
declare void @srand(i32) #1

declare i32 @time(...) #2

; Function Attrs: nounwind
declare i32 @rand() #1

declare i32 @printf(i8*, ...) #2

attributes #0 = { nounwind uwtable "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nounwind "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { nounwind }

!llvm.ident = !{!0}

!0 = metadata !{metadata !"clang version 3.5.2 (branches/release_35 232902)"}
